#!/usr/bin/env python3

TAG_DEFINE = '{http://relaxng.org/ns/structure/1.0}define'
TAG_ELEMENT = '{http://relaxng.org/ns/structure/1.0}element'
TAG_OPTIONAL = '{http://relaxng.org/ns/structure/1.0}optional'
TAG_INTERLEAVE = '{http://relaxng.org/ns/structure/1.0}interleave'
TAG_REF = '{http://relaxng.org/ns/structure/1.0}ref'
TAG_ZERO_OR_MORE = '{http://relaxng.org/ns/structure/1.0}zeroOrMore'
TAG_ATTRIBUTE = '{http://relaxng.org/ns/structure/1.0}attribute'
TAG_CHOICE = '{http://relaxng.org/ns/structure/1.0}choice'

from lxml.etree import ElementTree

f = open('gir-1.2.rng')
etree = ElementTree()
etree.parse(f)
root = etree.getroot()

def findDefine(root, name):
    for child in root.iter():
        if child.tag == TAG_DEFINE:
            if child.attrib['name'] == name:
                return child
    raise KeyError

def iterInterestingNodes(node):
    for child in node.iterchildren():
        if child.tag == TAG_ELEMENT:
            yield child
        elif child.tag == TAG_REF:
            yield child
        elif child.tag == TAG_ATTRIBUTE:
            yield child
        elif child.tag in (TAG_OPTIONAL, TAG_ZERO_OR_MORE, TAG_INTERLEAVE, TAG_CHOICE):
            for grandchild in iterInterestingNodes(child):
                yield grandchild

def getChildElements(node):
    for child in iterInterestingNodes(node):
        if child.tag == TAG_ELEMENT:
            yield child
        elif child.tag == TAG_REF:
            ref = findDefine(node.getroottree(), child.attrib['name'])
            for refchild in getChildElements(ref):
                yield refchild

def getClasses(node):
    for child in node.iterchildren():
        if child.tag == TAG_DEFINE:
            child = child.getchildren()[0]
            if child.tag == TAG_ELEMENT:
                yield child
            elif child.tag == TAG_INTERLEAVE:
                for descendant in iterInterestingNodes(child):
                    if descendant.tag == TAG_ELEMENT:
                        yield descendant

def getChildClasses(node):
    for child in iterInterestingNodes(node):
        if child.tag == TAG_ELEMENT:
            yield child

def getChildAttributes(node):
    for child in iterInterestingNodes(node):
        if child.tag == TAG_ATTRIBUTE:
            yield child
        elif child.tag == TAG_REF:
            ref = findDefine(node.getroottree(), child.attrib['name'])
            for refchild in getChildAttributes(ref):
                yield refchild

if __name__ == '__main__':
    for child in getClasses(root):
        print('class %s {' % child.attrib['name'])
        for attrib in getChildAttributes(child):
            print('  string %s;' % attrib.attrib['name'])
        for ele in getChildElements(child):
            print('  child %s;' % ele.attrib['name'])
        print('}')

        for grandchild in getChildClasses(child):
            print('class %s {' % grandchild.attrib['name'])
            for attrib in getChildAttributes(grandchild):
                print('  string %s;' % attrib.attrib['name'])
            for ele in getChildElements(grandchild):
                print('  child %s;' % ele.attrib['name'])
            print('}')



