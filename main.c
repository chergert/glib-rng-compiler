#include "rtfm-gir-parser.h"

gint
main (gint argc,
      gchar *argv[])
{
  const gchar *filename = argv[1];
  g_autoptr(GFile) file = g_file_new_for_commandline_arg (filename);
  g_autoptr(RtfmGirParser) parser = rtfm_gir_parser_new ();
  g_autoptr(GError) error = NULL;
  g_autoptr(RtfmGirRepository) repository = NULL;
  GString *str = NULL;

  repository = rtfm_gir_parser_parse_file (parser, file, NULL, &error);

  if (repository == NULL)
    {
      g_printerr ("%s\n", error->message);
      return 1;
    }

  str = g_string_new (NULL);
  rtfm_gir_parser_object_printf (RTFM_GIR_PARSER_OBJECT (repository), str, 0);
  g_print ("%s\n", str->str);
  g_string_free (str, TRUE);

  return 0;
}
