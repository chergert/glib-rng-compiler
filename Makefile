all: gir-parser

.generate.stamp: parser.py gir-1.2.rng
	./parser.py gir-1.2.rng RtfmGir rtfm_gir
	touch .generate.stamp

gir-parser: .generate.stamp main.c
	$(CC) -o $@ -Wall $(shell pkg-config --cflags --libs gio-2.0) main.c generated/*.c -I generated/

check: gir-parser
	@echo "Checking gir files in /usr/share/gir-1.0"
	@for girfile in `ls /usr/share/gir-1.0` ; do \
	  echo "  /usr/share/gir-1.0/$$girfile" ; \
	  ./gir-parser "/usr/share/gir-1.0/$$girfile" > /dev/null; \
	done

clean:
	rm -f gir-parser .generate.stamp
